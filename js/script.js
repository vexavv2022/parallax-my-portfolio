'use strict';
const switchers = document.querySelectorAll('.change-theme');
switchers.forEach(item => {
    item.addEventListener('click', function () {
        applyTheme(this.dataset.theme);
        localStorage.setItem('theme', this.dataset.theme);
    })
})

function applyTheme(themeName) {
    let themeUrl = `css/theme-${themeName}.css`;
    document.querySelector('[title="theme"]').setAttribute('href', themeUrl);
}

let activeThem = localStorage.getItem('theme');
if (activeThem === null) {
    applyTheme('light');
} else {
    applyTheme(activeThem);
}
// слайдер на три позиции
let ofset = 0;
const sliderLine = document.querySelector('.main__our-team');
const btnNext = document.querySelector('.btn-next');
const btnPrev = document.querySelector('.btn-prev');

btnNext.addEventListener('click', () => {
    ofset = ofset + 1100;
    if (ofset > 2200) {
        ofset = 0
    }
    sliderLine.style.left = -ofset + 'px'
})
btnPrev.addEventListener('click', () => {
    ofset = ofset - 1100;
    if (ofset < 0) {
        ofset = 2200
    }
    sliderLine.style.left = -ofset + 'px'
})